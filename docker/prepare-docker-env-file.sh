#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ENV_FILE=${SCRIPT_DIR}'/../.env'
PROJECT_NAME_FILE=${SCRIPT_DIR}'/../project.name'

function _getProjectName() {
    project_name=`cat ${PROJECT_NAME_FILE}`

    echo ${project_name}
}


[ -f ${ENV_FILE} ] && rm ${ENV_FILE}
echo PROJECT_NAME=`_getProjectName` >> ${ENV_FILE}
echo GID=$(id -g) >> ${ENV_FILE}
echo UID=$(id -u) >> ${ENV_FILE}
